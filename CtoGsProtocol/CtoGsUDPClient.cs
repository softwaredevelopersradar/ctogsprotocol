﻿using CtoGsProtocol;
using llcss;
using Nito.AsyncEx;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;


namespace CtoGsProtocol
{
    public class CtoGsUDPClient
    {
        private string localAddress; //локальный адрес для прослушивания входящих подключений
        private int localPort; // локальный порт для прослушивания входящих подключений

        private string remoteAddress; // хост для отправки данных
        private int remotePort; // порт для отправки данных

       
        private byte ReceiverAddress;

        private readonly AsyncLock asyncLock;

        private readonly ConcurrentDictionary<int, ConcurrentQueue<TaskCompletionSource<IBinarySerializable>>> concurrentDictionary;

        private bool _IsConnected = false;
        public bool IsConnected
        {
            get { return _IsConnected; }
            set
            {
                _IsConnected = value;
                OnConnected?.Invoke(value);
            }
        }

        private void IsConnectedChange(bool value)
        {
            IsConnected = value;
        }


        public delegate void OnConnectedEventHandler(bool onConnected);
        public event OnConnectedEventHandler OnConnected;

        public delegate void OnReadEventHandler(bool onRead);
        public event OnReadEventHandler OnRead;

        public delegate void OnWriteEventHandler(bool onWrite);
        public event OnWriteEventHandler OnWrite;

        public CtoGsUDPClient()
        {
            asyncLock = new AsyncLock();
            concurrentDictionary = new ConcurrentDictionary<int, ConcurrentQueue<TaskCompletionSource<IBinarySerializable>>>();
        }

        static byte OwnServerAddress = 0;
        static MessageHeader GetMessageHeader(int code, int length)
        {
            return new MessageHeader(OwnServerAddress, 1, (byte)code, 0, length);
        }

        private UdpClient sender;

        public void ConnectToUDP(string localAddress, int localPort, string remoteAddress, int remotePort)
        {
            sender = new UdpClient(); // создаем UdpClient для отправки сообщений

            this.localAddress = localAddress;
            this.localPort = localPort;
            this.remoteAddress = remoteAddress;
            this.remotePort = remotePort;

            Console.WriteLine("UDPConnect");

            OnConnectedTask();

            Task.Run(() => ReadThread());

            IsConnectedChange(true);
        }

        public void DisconnectFromServer()
        {
            IsConnectedChange(false);
            sender.Close();
        }

        private void OnConnectedTask()
        {
            var taskQueues = concurrentDictionary.Values.ToArray();
            foreach (var queue in taskQueues)
            {
                foreach (var task in queue)
                {
                    //task.SetCanceled();
                    task.SetResult(null);
                }
            }
            concurrentDictionary.Clear();
        }

        //Core
        private async Task ReadThread()
        {
            UdpClient receiver = new UdpClient(localPort);
            while (IsConnected)
            {
                var headerBuffer = new byte[MessageHeader.BinarySize];
                var header = new MessageHeader();
                byte[] Buffer = new byte[MessageHeader.BinarySize];
                try
                {
                    UdpReceiveResult udpReceiveResult = await receiver.ReceiveAsync();
                    Buffer = udpReceiveResult.Buffer;
                    Array.Copy(udpReceiveResult.Buffer, headerBuffer, headerBuffer.Length);
                    OnRead?.Invoke(true);
                }
                catch (Exception)
                {
                    //Console.WriteLine("Сервер отвалился");
                    OnRead?.Invoke(false);
                    IsConnectedChange(false);
                }

                var b00l = MessageHeader.TryParse(headerBuffer, out header);
                if (b00l == false) Console.WriteLine("AWP: Critical Error: .TryParse");

                switch (header.Code)
                {
                    case 0:
                        ReceiverAddress = header.ReceiverAddress;
                        break;
                    case 1:
                        HandleUDPEvent<CuirasseMessage>(header, Buffer);
                        break;
                    case 2:
                        HandleUDPEvent<GrozaSMessage>(header, Buffer);
                        break;
                    case 3:
                        HandleUDPEvent<DeleteMessage>(header, Buffer);
                        break;
                    case 4:
                        HandleUDPEvent<TypeAndCoordsMessage>(header, Buffer);
                        break;

                    default:
                        Console.WriteLine("AWP: Unknown Code: " + header.Code);
                        break;
                }
            }
            receiver.Close();
        }

        private T ReadUDPResponse<T>(MessageHeader header, byte[] Buffer) where T : class, IBinarySerializable, new()
        {
            try
            {
                var result = new T();
                result.Decode(Buffer, 0);
                return result;
            }
            catch (Exception e)
            {
                var с = e.ToString();
                return null;
            }
        }

        #region Events

        //Шифр 1
        public delegate void GetCuirasseMessageEventHandler(CuirasseMessage answer);
        public event GetCuirasseMessageEventHandler GetCuirasseMessage;

        //Шифр 2
        public delegate void GetGrozaSMessageEventHandler(GrozaSMessage answer);
        public event GetGrozaSMessageEventHandler GetGrozaSMessage;

        //Шифр 3
        public delegate void GetDeleteMessageEventHandler(DeleteMessage answer);
        public event GetDeleteMessageEventHandler GetDeleteMessage;

        //Шифр 4
        public delegate void GetTypeAndCoordsMessageEventHandler(TypeAndCoordsMessage answer);
        public event GetTypeAndCoordsMessageEventHandler GetTypeAndCoordsMessage;

        #endregion

        private void HandleUDPEvent<T>(MessageHeader header, byte[] Buffer) where T : class, IBinarySerializable, new()
        {
            var responseEvent = ReadUDPResponse<T>(header, Buffer);

            if (responseEvent is CuirasseMessage)
            {
                var answer = responseEvent as CuirasseMessage;
                GetCuirasseMessage?.Invoke(answer);
            }

            if (responseEvent is GrozaSMessage)
            {
                var answer = responseEvent as GrozaSMessage;
                GetGrozaSMessage?.Invoke(answer);
            }

            if (responseEvent is DeleteMessage)
            {
                var answer = responseEvent as DeleteMessage;
                GetDeleteMessage?.Invoke(answer);
            }

            if (responseEvent is TypeAndCoordsMessage)
            {
                var answer = responseEvent as TypeAndCoordsMessage;
                GetTypeAndCoordsMessage?.Invoke(answer);
            }

            if (responseEvent == null)
            {
                Console.WriteLine("AWP: responseEvent == null");
            }
        }

        private async Task SendRequestMini(MessageHeader header, byte[] message)
        {
            using (await asyncLock.LockAsync().ConfigureAwait(false))
            {
                int count = message.Count();
                try
                {
                    await sender.SendAsync(message, message.Length, remoteAddress, remotePort); // отправка
                    OnWrite?.Invoke(true);
                }
                catch (Exception ex)
                {
                    OnWrite?.Invoke(false);
                }
            }
        }

        //Шифр 1 
        public async Task<DefaultMessage> SendUAVtoGs(UAV uav)
        {
            if (IsConnected)
            {
                MessageHeader header = GetMessageHeader(code: 1, length: UAV.BinarySize);
                header.SenderAddress = ReceiverAddress;
                header.ReceiverAddress = 0;

                byte[] message = CuirasseMessage.ToBinary(header, uav);

                await SendRequestMini(header, message);

                header = new MessageHeader(0, 0, 1, 0, 0);
                DefaultMessage answer = new DefaultMessage(header);
                return answer;
            }
            else
            {
                MessageHeader header = new MessageHeader(0, 0, 1, 1, 0);
                DefaultMessage answer = new DefaultMessage(header);
                return answer;
            }
        }

        //Шифр 2
        public async Task<DefaultMessage> SendDronetoC(Drone drone)
        {
            if (IsConnected)
            {
                MessageHeader header = GetMessageHeader(code: 2, length: Drone.BinarySize);
                header.SenderAddress = ReceiverAddress;
                header.ReceiverAddress = 0;

                byte[] message = GrozaSMessage.ToBinary(header, drone);

                await SendRequestMini(header, message);

                header = new MessageHeader(0, 0, 2, 0, 0);
                DefaultMessage answer = new DefaultMessage(header);
                return answer;
            }
            else
            {
                MessageHeader header = new MessageHeader(0, 0, 2, 1, 0);
                DefaultMessage answer = new DefaultMessage(header);
                return answer;
            }
        }

        //Шифр 3
        public async Task<DefaultMessage> SendDeleteMessage(int ID)
        {
            if (IsConnected)
            {
                MessageHeader header = GetMessageHeader(code: 3, length: DeleteMessage.BinarySize - MessageHeader.BinarySize);
                header.SenderAddress = ReceiverAddress;
                header.ReceiverAddress = 0;

                byte[] message = DeleteMessage.ToBinary(header, ID);

                await SendRequestMini(header, message);

                header = new MessageHeader(0, 0, 3, 0, 0);
                DefaultMessage answer = new DefaultMessage(header);
                return answer;
            }
            else
            {
                MessageHeader header = new MessageHeader(0, 0, 3, 1, 0);
                DefaultMessage answer = new DefaultMessage(header);
                return answer;
            }
        }

        //Шифр 4
        public async Task<DefaultMessage> SendTypeAndCoordsMessage(TypeAndCoords[] typeAndCoords)
        {
            if (IsConnected)
            {
                MessageHeader header = GetMessageHeader(code: 4, length: typeAndCoords.Length * TypeAndCoords.BinarySize);
                header.SenderAddress = ReceiverAddress;
                header.ReceiverAddress = 0;

                byte[] message = TypeAndCoordsMessage.ToBinary(header, typeAndCoords);

                await SendRequestMini(header, message);

                header = new MessageHeader(0, 0, 4, 0, 0);
                DefaultMessage answer = new DefaultMessage(header);
                return answer;
            }
            else
            {
                MessageHeader header = new MessageHeader(0, 0, 4, 1, 0);
                DefaultMessage answer = new DefaultMessage(header);
                return answer;
            }
        }
    }
}
