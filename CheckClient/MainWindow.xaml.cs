﻿using CtoGsProtocol;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CheckClient
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        CtoGsUDPClient ctoGsUDPClient = new CtoGsUDPClient();

        CtoGsUDPClient ctoGsUDPClient2 = new CtoGsUDPClient();

        public MainWindow()
        {
            InitializeComponent();
            InitEvents();
        }

        private void InitEvents()
        {
            ctoGsUDPClient.GetCuirasseMessage += CtoGsUDPClient_GetCuirasseMessage;
            ctoGsUDPClient.GetGrozaSMessage += CtoGsUDPClient_GetGrozaSMessage;

            ctoGsUDPClient2.GetCuirasseMessage += CtoGsUDPClient2_GetCuirasseMessage;
            ctoGsUDPClient2.GetGrozaSMessage += CtoGsUDPClient2_GetGrozaSMessage;
        }

        private void CtoGsUDPClient_GetCuirasseMessage(CuirasseMessage answer)
        {
            Console.Beep();
        }

        private void CtoGsUDPClient_GetGrozaSMessage(GrozaSMessage answer)
        {
            Console.Beep();
        }

        private void CtoGsUDPClient2_GetCuirasseMessage(CuirasseMessage answer)
        {

        }

        private void CtoGsUDPClient2_GetGrozaSMessage(GrozaSMessage answer)
        {

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            ctoGsUDPClient.ConnectToUDP("127.0.0.1", 8002, "127.0.0.1", 8001);
        }

        private async void Button_Click_1(object sender, RoutedEventArgs e)
        {
            UAV uAV = new UAV() { UAVDrone = new Drone() { ID = 23, FrequencyMHz = 2563, BandwidthMHz=23, Type=(DroneType)2},
            UAVCoords = new Coords() { Latitude = 27.568923, Longitude = 54.235689, Altitude = 235}
            };
            await ctoGsUDPClient.SendUAVtoGs(uAV);
        }

        private async void Button_Click_2(object sender, RoutedEventArgs e)
        {
            Drone drone = new Drone();
            await ctoGsUDPClient.SendDronetoC(drone);
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            ctoGsUDPClient2.ConnectToUDP("127.0.0.1", 8001, "127.0.0.1", 8002);
        }

        private async void Button_Click_4(object sender, RoutedEventArgs e)
        {
            UAV uAV = new UAV()
            {
                UAVDrone = new Drone() { ID = 23, FrequencyMHz = 2563, BandwidthMHz = 23, Type = (DroneType)2 },
                UAVCoords = new Coords() { Latitude = 54.568923, Longitude = 26.235689, Altitude = 235 }
            };
            await ctoGsUDPClient2.SendUAVtoGs(uAV);
        }

        private async void Button_Click_5(object sender, RoutedEventArgs e)
        {
            Drone drone = new Drone();
            await ctoGsUDPClient2.SendDronetoC(drone);
        }
    }
}
